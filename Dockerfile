FROM node:20.11.1
# FROM ubuntu:22.04.4
# RUN apt-get install git
WORKDIR /app
COPY . .
RUN npm install
RUN pwd
CMD [ "node", "app.js" ]
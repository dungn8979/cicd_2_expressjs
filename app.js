const express = require('express')
const app = express()
const port = 3000

app.use('/', (req, res, next) => {
    console.log('into /');
    res.send('Init project')
})

app.use('/tmp', (req, res, next) => {
    console.log('into /tmp');
    res.send('Init tmp')
})

app.listen(port, () => {
    console.log('a init project in port:', port);
})